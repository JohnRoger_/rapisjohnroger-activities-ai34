export const toast = {
    methods: {
        showSuccessToast(msg){
            this.$toast.success(msg, {
                position: 'bottom-left',
                timeout: 3000,
                closeOnClick: true,
                pauseOnFocusLoss: false,
                pauseOnHover: false,
                draggable: true,
                draggablePercent: 0.6,
                showCloseButtonOnHover: false,
                hideProgressBar: true,
                closeButton: 'button',
                icon: true,
                rtl: false,
             });
},

        showErrorToast(msg){
            this.$toast.error(msg, {
                position: 'bottom-left',
                timeout: 3000,
                closeOnClick: true,
                pauseOnFocusLoss: false,
                pauseOnHover: false,
                draggable: true,
                draggablePercent: 0.6,
                showCloseButtonOnHover: false,
                hideProgressBar: true,
                closeButton: 'button',
                icon: true,
                rtl: false,

            });

},
        showWarningToast(msg){
                this.$toast.warning(msg, {
                    position: 'bottom-left',
                    timeout: 5000,
                    closeOnClick: true,
                    pauseOnFocusLoss: false,
                    pauseOnHover: false,
                    draggable: true,
                    draggablePercent: 0.6,
                    showCloseButtonOnHover: false,
                    hideProgressBar: false,
                    closeButton: 'button',
                    icon: true,
                    rtl: false,

            });
    
}
}
}