import Vue from 'vue'
import Vuex from 'vuex'
import axios from '../../config'

Vue.use(Vuex);

const Borrowbook = 'borrowedbooks'

export default ({
  namespaced: true,
  state: {
  },

  getters: {
  },

  mutations: {
    updateCopies(state, {id, data}){state.books[id].copies = data}
  },

  actions: {
    async storeBorrowed({commit}, {data, index}){
      const res = await axios.post(`${Borrowbook}`, data)
      .then(response => {
        commit('updateCopies', {id: index, data: response.data.Borrowbooks.books.copies}, {root: true})
        return response
      })
      .catch(error => {
        return error.response
      })

      return res;
    }
  },
})