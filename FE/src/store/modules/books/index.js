import Vue from 'vue'
import Vuex from 'vuex'
import axios from '../../config'

Vue.use(Vuex);

const Category = 'categories'
const Books = 'books'

export default {
  namespaced: true,

  state: {
    books: [],
    categories: [],
  },

  getters: {
    getBooksData(state){
      return state.books;
    }
  },

  mutations: {
    setCategories: (state, categories) => (state.categories = categories),
    setBooks: (state, books) => (state.books = books),
    deleteBooks(state, id){state.books = state.books.filter(book => { return book.id !== id;});},
    saveBook: (state, book) => state.books.unshift({ ...book }),
    updateBook(state, {id, data}){ Vue.set(state.books, id, data);},
    },

  actions: {
    async getCategories({ commit }){
      const res = await axios.get(Category).then(response => {
        commit("setCategories", response.data)
        return response
      })
      .catch(error => {
        return error.response
      }); 
      return res;
    },
    
    async storeBook({ commit }, book) {
      const response = await axios.post("/books", book).then((res) => {
        commit("saveBook", res.data.book);
          return res;
        })
        .catch((err) => {
          return err.response;
        });
      return response;
    },

    async deleteBook({ commit }, id) {
      const response = await axios.delete(`/books/${id}`).then((res) => {
        commit("deleteBooks", id);
          return res;
        })
        .catch((err) => {
          return err.response;
        });
      return response;
    },

    async getBooks({ commit }) {
      await axios.get(Books).then((res) => {
        commit("setBooks", res.data);
        })
        .catch((err) => {
          console.error(err);
        });
    },

    async updateBook({ commit }, {data, index }){
      console.log(data, index)
      const res = await axios.put(`${Books}/${data.id}`, data).then((response) => {
        commit("updateBook", {index, data});
          return response;
        })
        .catch((error) => {
          return error.response;
        });
        return res;
      }
    },

};
