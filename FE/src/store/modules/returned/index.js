//import anai an vue ngan next na an vuex
import Vue from 'vue'
import Vuex from 'vuex'
import axios from '../../config'

Vue.use(Vuex);

const Returnedbook = 'returnedbooks'

export default ({
  namespaced: true,
  state: {
  },
  getters: {
 
  },
  mutations: {
    updateCopies(state, {id, data}){state.books[id].copies = data}
  },

  actions: {
    async createReturned({commit}, {data, index}){
      const res = await axios.post(`${Returnedbook}`, data).then(response => {
        commit('updateCopies', {id: index, data: response.data.book.book.copies}, {root: true})
        return response
      })
      .catch(error => {
        return error.response
      })

      return res;
    }
  },
})