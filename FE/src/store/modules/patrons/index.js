import Vue from 'vue'
import Vuex from 'vuex'
import axios from '../../config'

Vue.use(Vuex);

const Patron = 'patrons'

export default ({
  namespaced: true,

  state: {
    patrons: [],
  },

  getters: {getPatronData(state){
    return state.patrons;
  }
},

  mutations: {
    setPatron: (state, patrons) => (state.patrons = patrons),
    savePatron: (state, patron) => state.patrons.unshift({ ...patron }),
    deletePatrons(state, id){state.patrons = state.patrons.filter(patron => {return patron.id !== id;});},
    updatePatron(state, {id, data}){ Vue.set(state.patrons, id, data);},
    },

  actions: {
    async getPatrons({ commit }) {
      await axios.get(Patron).then((res) => {
        commit("setPatron", res.data);
        })
        .catch((err) => {
          console.error(err);
        });
    },

    async storePatron({ commit }, patron){
      const response = await axios.post("/patrons", patron).then(res => {
        commit('savePatron', res.data.patron)
          return res;
        })
        .catch(err => {
          return err.response;
        });
        return response;
      },
      
    async deletePatron({ commit }, id) {
      const response = await axios.delete(`/patrons/${id}`).then((res) => {
        commit("deletePatrons", id);
          return res;
        })
        .catch((err) => {
          return err.response;
        });
      return response;
    },

    async updatePatron({ commit }, {data, index}){
      console.log('UPDATE', data, 'INDEX', index)
        const res = await axios.put(`${Patron}/${data.id}`, data).then(response => {
          commit('updatePatron', {index, data})
            return response;
        })
          .catch((error) => {
            return error.response;
        });
        return res;
      }
    },
})