import Vue from 'vue';
import Vuex from 'vuex'; 

Vue.use(Vuex); 

import books from './modules/books/index'; 
import patrons from './modules/patrons/index';
import borrowed from './modules/borrowed/index';
import returned from './modules/returned/index';

export default new Vuex.Store({
    modules:{
        books, 
        patrons,
        borrowed, 
        returned
    }
}) 