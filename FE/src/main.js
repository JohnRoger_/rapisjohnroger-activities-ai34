import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import Vuex from 'vuex';
import BootstrapVue from 'bootstrap-vue'
import Toast from "vue-toastification";
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

import "vue-toastification/dist/index.css";

Vue.use(Toast);

Vue.use(BootstrapVue);

Vue.use(Vuex);

Vue.config.productionTip = false

new Vue({
  render: h => h(App), 
  router, 
  store
}).$mount('#app')