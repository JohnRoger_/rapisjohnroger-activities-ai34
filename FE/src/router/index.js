import Vue from 'vue'
import Router from 'vue-router'
import Dashboard from '../components/pages/Dashboard.vue'
import Patron from '../components/pages/Patron.vue'
import Book from '../components/pages/Book.vue' 
import Setting from '../components/pages/Setting.vue'  

Vue.use(Router);

const routes = [
	{
		path: '/',
		name: 'Dashboard',
		component: Dashboard
	},
    {
       path: '/Patron',
        name: 'Patron',
        component: Patron
    },
    {
        path: '/Book',
        name: 'Book',
        component: Book
    },
    {
        path: '/Setting',
        name: 'Setting',
        component: Setting
    }
]
const router = new Router({
  routes
})

export default router