Chart.defaults.global.defaultFontFamily = 'Poppins';
Chart.defaults.global.defaultFontSize = 15;

 let myChart = document.getElementById('myChart').getContext('2d');
 let aChart = new Chart(myChart, {
  type: 'bar',
  data: {
    labels: ['JAN', 'FEB', 'MARCH', 'APRIL', 'MAY', 'JUNE', 'JULY', 'AUG', 'SEPT', 'OCT', 'NOV', 'DEC'],
    datasets: [
    {
      label: 'Books',
      data: [3, 7, 8, 9, 9, 8, 10, 6, 9, 15, 11, 11],
      backgroundColor: "rgba(96, 160, 280, 1)",
    }
  ],
  },
  options: {
    scales: {
      yAxes: [{
        ticks: {
        }
      }]
    }
  }
});

let myChart2 = document.getElementById('myChart2').getContext('2d');
 let bChart = new Chart(myChart2, {
  type: 'doughnut',
  data: {
    labels: ['Comedy', 'Horror', 'History', 'Educational', 'Sci-Fi', 'Greek mythology'],
    datasets: [
    {
      label: 'Borrowed Books',
      data: [7, 10, 9, 12, 13, 6],
      backgroundColor: ['rgba(134, 234, 233, 1)',
                        'rgba(93, 189, 211, 1)',
                        'rgba(69, 145, 184, 1)',
                        'rgba(59, 102, 150, 1)',
                        'rgba(53, 60, 110, 1)',
                        'rgba(112, 87, 136, 1)',
    ],
    }
  ],
  },
  options: {
    scales: {
      yAxes: [{
        ticks: {
        }
      }]
    }
  }
});