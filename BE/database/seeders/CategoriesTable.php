<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CategoriesTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->insert([
            'category' => 'Horror' 
        ]);
        DB::table('categories')->insert([
            'category' => 'Sci-Fi' 
        ]);
        DB::table('categories')->insert([
            'category' => 'History' 
        ]);
        DB::table('categories')->insert([
            'category' => 'Greek Mythology' 
        ]);
        DB::table('categories')->insert([
            'category' => 'Romance' 
        ]);
        DB::table('categories')->insert([
            'category' => 'Educational' 
        ]);
    }
}
