<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Patrons extends Model
{
    use HasFactory;

    protected $fillable = [
        'last_name',
        'first_name',
        'middle_name', 
        'email',
    ];

    public function returnedbooks(){
        return $this->hasMany(ReturnedBooks::class, 'returned_books', 'patron_id','id');
    }
    
    public function borrowedbooks(){
        return $this->hasMany(BorrowedBooks::class, 'borrowed_books', 'patron_id','id');
    }
}
