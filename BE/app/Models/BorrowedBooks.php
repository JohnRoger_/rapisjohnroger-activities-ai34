<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BorrowedBooks extends Model
{
    use HasFactory;

    protected $fillable = [
        'patron_id', 
        'copies', 
        'book_id',
    ];

    public function patrons(){
        return $this->belongsTo(Patrons::class, 'patron_id', 'id');
    }

    public function books(){
        return $this->belongsTo(Books::class, 'book_id', 'id');
    }
    
}
