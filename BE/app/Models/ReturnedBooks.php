<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ReturnedBooks extends Model
{
    use HasFactory;

    protected $fillable = [
        'book_id', 
        'copies', 
        'patron_id', 
    ];


    public function patrons(){
        return $this->belongsTo(Patrons::class, 'patron_id', 'id');
    }

    public function books(){
        return $this->belongsTo(Books::class, 'book_id', 'id');
    }

}
