<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Books extends Model
{
    use HasFactory;

    protected $fillable = [
        'name', 
        'author', 
        'copies', 
        'category_id',
    ];

    public function categories(){
        return $this->hasOne(Categories::class, 'id', 'category_id'); 
    } 

    public function borrowedbooks(){
        return $this->hasMany(BorrowedBooks::class, 'book_id', 'id'); 
    }
    
    public function returnedbooks(){
        return $this->hasMany(ReturnedBooks::class, 'book_id', 'id'); 
    }


}
