<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreBorrowRequest;
use App\Models\BorrowedBooks;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;

class BorrowedBooksController extends Controller
{
    public function index(){
        $borrowedbooks = BorrowedBooks::all();
        return response()->json(['data' => $borrowedbooks], 200);
    }
    
    public function show($id)
    {
        $borrowedbooks = BorrowedBooks::with(['patron', 'book', 'book.category'])->where('id', $id)->firstOrFail();
        return response()->json($borrowedbooks);
    }

    public function store(StoreBorrowRequest $request){
        $storeborrowed = BorrowedBooks::create($request->only(['book_id', 'copies', 'patron_id']));

        $borrowedbooks = BorrowedBooks::with(['books'])->find($storeborrowed->id);
        $copies = $borrowedbooks->books->copies - $request->copies;
        $borrowedbooks->books->update(['copies' => $copies]);

        return response()->json(['message' => 'Book borrowed successfully', 'borrowedbook' => $borrowedbooks]);
    }
}
