<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Patrons;
use App\Http\Requests\StorePatronsRequest;



class PatronsController extends Controller
{

    public function index()
    {
        $patrons = Patrons::all();
        return response()->json($patrons);
    }

    public function store(StorePatronsRequest $request)
    {
        $patrons = Patrons::create($request->validated());

        return response()->json(['message' => 'Patron saved successfully!', 'patron' => $patrons], 200);
    }

    public function show($id)
    {
        return response()->json(Patrons::findOrFail($id));
    }

    public function update(StorePatronsRequest $request, $id)
    {
            $patrons = Patrons::findOrFail($id);
            $patrons->update($request->validated());

            return response()->json(['message' => 'Patron updated', 'patron' => $patrons], 200);
    }

    public function destroy($id)
    {
            $patrons = Patrons::where('id', $id)->firstOrFail();
            $patrons->delete();

            return response()->json(['message' => 'Patron deleted successfully!'], 200);
    }
}
