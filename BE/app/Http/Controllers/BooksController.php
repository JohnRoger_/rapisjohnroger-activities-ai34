<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Books;
use App\Http\Requests\StoreBooksRequest;



class BooksController extends Controller
{

    public function index()
    {
        Books::all();
        return response()->json(Books::with(['categories'])->get());
        
    }

    public function store(StoreBooksRequest $request)
    {
        $save = Books::create($request->validated());
		$books = Books::with(['categories'])->find($save->id);

        return response()->json(['message' => 'Book saved successfully!', 'book' => $books], 200);
    }

    public function show($id)
    {
        $books = Books::with(['categories'])->where('id', $id)->firstOrFail();

        return response()->json($books);
    }

    public function update(StoreBooksRequest $request, $id)
    {
        $books = Books::with(['categories'])->where('id', $id)->firstOrFail();
        $books->update($request->validated());
        $updatedbook = Books::with(['categories'])->where('id', $id)->firstOrFail();
        
        return response()->json(['message' => 'Book updated successfully!', 'book' => $updatedbook], 200);
    }

    public function destroy($id)
    {
        $books = Books::where('id', $id)->firstOrFail();
        $books->delete();

        return response()->json(['message' => 'Book deleted successfully!'], 200);
    }
}
