<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Books;
use App\Models\BorrowedBooks;
use App\Models\ReturnedBooks;
use App\Http\Requests\StoreReturnRequest;

class ReturnedBooksController extends Controller
{
     public function index()
    {
        $returnedbooks = BorrowedBooks::all();
        return response()->json(['data' => $returnedbooks], 200);
    }

    public function store(StoreReturnRequest $request)
    {
        $borrowedbooks = BorrowedBooks::where([['book_id', $request->book_id],['patron_id', $request->patron_id],])->firstOrFail();
        
        if(!empty($borrowedbooks))
        {
            if($borrowedbooks->copies == $request->copies){
                $borrowedbooks->delete();
            }
            else
            {
                $borrowedbooks->update(['copies' => $borrowedbooks->copies - $request->copies]);
            }            
            $create_returned = ReturnedBooks::create($request->only(['book_id', 'copies', 'patron_id']));
            $returnedbooks = ReturnedBooks::with(['books'])->find($create_returned->id);
            $copies = $returnedbooks->books->copies + $request->copies;
            $returnedbooks->books->update(['copies' => $copies]);

            return response()->json(['message' => 'Book returned successfully!', 'book' => $returnedbooks]);
        }
    }

    public function show($id)
    {
        $returnedbooks = ReturnedBooks::with(['book', 'book.category', 'patron'])->findOrfail($id);
        return response()->json($returnedbooks);
    }

}
