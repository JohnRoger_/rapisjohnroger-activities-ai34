<?php

namespace App\Http\Requests;

use App\Models\Books;
use Illuminate\Foundation\Http\FormRequest;

class StoreBorrowRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $books = Books::find(request()->get('book_id'));
        if(!empty($books)){
            $copies = $books->copies;
        }
        else{
            $copies = request()->get('copies');
        }
        return [
            'book_id' => 'bail|required|exists:books,id',
            'copies' => ['required',"lte: {$copies}", 'bail', 'gt:0'],
            'patron_id' => 'exists:patrons,id',
        ];
    
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'book_id.exists' => 'The book doesn\'t exist in the database',
            'copies.lte' => 'The borrowed copies given exceeded the total copies of book',
            'patron_id.exists' => 'The patron doesn\'t exist in the database'
        ];
    }
}