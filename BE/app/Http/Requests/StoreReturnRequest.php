<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Models\BorrowedBooks;

class StoreReturnRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $books = BorrowedBooks::where('book_id', request()->get('book_id'))->where('patron_id', request()->get('patron_id'))->first();
        if (!empty($books)) {
            $copies = $books->copies;
        } else {
            $copies = request()->get('copies');
        }
        return [
            'book_id' => ['required', 'numeric'],
            'copies' => ['required', 'numeric', 'max:'.$copies],
            'patron_id' => ['required', 'numeric']
        ];

    }

    public function messages()
{
        return [
            'book_id.required' => 'Book ID is required',
            'book_id.numeric' => 'Book should be in integer format',

            'copies.required' => 'Copies is required',
            'copies.numeric' => 'Copies should be in integer format',
            'copies.max' => 'Copies should not exceed available copies',
            'copies.min' => 'Copies cannot be zero ',

            'patron_id.required' => 'Patron ID is required',
            'patron_id.numeric' => 'Patron ID should be in integer format',
        ];
    }
}
