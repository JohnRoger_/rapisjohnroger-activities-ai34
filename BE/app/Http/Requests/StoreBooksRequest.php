<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreBooksRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name' => ['required', 'regex:/^[a-zA-ZÑñ\s]+$/'],
            'author' => ['required', 'regex:/^[a-zA-ZÑñ\s]+$/'],
            'copies' => ['required', 'numeric', 'gt:0'],
            'category_id' => ['required', 'numeric', 'gt:0']
        ];
    }

    public function messages()
{
    return [
        'name.required' => 'Name field is required',
        'name.regex' => 'Name should be in string format',

        'author.required' => 'Author field is required',
        'author.regex' => 'Author should be in string format',

        'copies.required' => 'Copies field is required',
        'copies.numeric' => 'Copies should be in integer format',
        'copies.gt' => 'Copies should not be 0',

        'category_id.required' => 'Category ID is required',
        'category_id.numeric' => 'Category ID should be in integer format',
        'category.gt' => 'Category required!',
    ];
}
}
