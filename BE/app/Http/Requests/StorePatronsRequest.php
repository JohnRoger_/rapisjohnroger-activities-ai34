<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;


class StorePatronsRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
            return [
                'last_name' => ['required', 'regex:/^[a-zA-ZÑñ\s]+$/'],
                'first_name' => ['required', 'regex:/^[a-zA-ZÑñ\s]+$/'],
                'middle_name' => ['required', 'regex:/^[a-zA-ZÑñ\s]+$/'],
                'email' => ['required', 'email']
        ];
    }
    public function messages()
{
    return [
        'last_name.required' => 'Last Name field is required',
        'last_name.regex' => 'Last Name should be in string format',

        'first_name.required' => 'First Name field is required',
        'first_name.regex' => 'First Name field should be in string format',

        'middle_name.required' => 'Middle Name field is required',
        'middle_name.regex' => 'Middle Name field should be in string format',

        'email.required' => 'Email field is required',
        'email.unique' => 'Email already used by other users',
        'email.email' => 'Email must be valid',
    ];
    
}
}