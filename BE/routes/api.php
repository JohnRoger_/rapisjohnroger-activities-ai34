<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\BooksController;
use App\Http\Controllers\CategoriesController;
use App\Http\Controllers\BorrowedBooksController;
use App\Http\Controllers\ReturnedBooksController;
use App\Http\Controllers\PatronsController;

Route::resource('books', BooksController::class)->only(['index', 'store', 'show', 'update', 'destroy']);
Route::get('categories', [CategoriesController::class, 'index']);

Route::resource('/borrowedbooks', BorrowedBooksController::class);
Route::resource('/returnedbooks', ReturnedBooksController::class);

Route::resource('patrons', PatronsController::class)->only(['index', 'store', 'show', 'update', 'destroy']);


